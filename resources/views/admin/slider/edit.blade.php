@extends('admin.master')
@section('title','Sửa ảnh ')
@section('content')
    @if(count($errors)>0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <section class="content-header">
        <h1>
            Quản lí slider
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Trang chủ</a></li>
            <li class="active">Sửa ảnh</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Sửa ảnh</h3>
                    </div>
                    <form role="form" action="{{route('postEditSlider',['id'=>$data['id']])}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Ảnh</label>
                                <input type="file" name="txt_img" placeholder="name">
                                <input type="hidden" name="txt_img_current" value="{{$data['link']}}"><br />
                                <img src="{{asset('/uploads/admin/slider/'.$data['link'])}}"  alt="" width="100px">
                            </div>
                            <div class="form-group">
                                <label for="">Trạng thái</label>
                                <select class="form-control" name="slt_status">
                                    @if($data['status']==1)
                                        <option value="1" selected>Hiện</option>
                                        <option value="0">Ẩn</option>
                                    @else
                                        <option value="1">Hiện</option>
                                        <option value="0"  selected>Ẩn</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <a href="{{route('getListSlider')}}"><button type="button" class="btn btn-danger pull-right " style="margin-left: 5px">Cancel</button></a>
                            <button type="submit" class="btn btn-primary pull-right">Sửa</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection