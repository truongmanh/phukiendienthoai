@extends('login.master')
@section('title','login')
@section('content')
    <div class = "container">
        <div class="wrapper">
           
            <form action="{{route('login')}}" method="post" name="Login_Form" class="form-signin" id="formli">
                {{csrf_field()}}
                <h3 class="form-signin-heading">Đăng nhập</h3>
                @if(Session::has('thong bao'))
            <div class="errors-loginn">
                <h5>{{Session::get('thong bao')}}</h5>
            </div>
             @endif
                <hr class="colorgraph"><br>
                <input type="text" class="form-control" name="txt_name" placeholder="Username" required="" autofocus="" />
                <input type="password" class="form-control" name="txt_pass" placeholder="Password" required=""/>
                <button class="btn btn-lg btn-primary btn-block"  name="Submit" value="Login" type="Submit">Đăng nhập</button>
            </form>
        </div>
    </div>
    @endsection