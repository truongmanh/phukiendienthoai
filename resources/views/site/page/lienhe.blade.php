@extends('site.master')
@section('title','liên hệ')
@section('content')
    <div class="inner-header">
        <div class="container">
            <div class="pull-left">
                <h6 class="inner-title">Contacts</h6>
            </div>
            <div class="pull-right">
                <div class="beta-breadcrumb font-large">
                    <a href="{{route('trang-chu')}}">Trang chủ</a> / <span>Liên hệ</span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="beta-map">

        <div class="abs-fullwidth beta-map wow flipInX"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d779.9052694062914!2d105.84535881030209!3d21.590779136926145!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x313526e96fe87bad%3A0x659e09d16c0f1867!2zMjggUGhhbiDEkMOsbmggUGjDuW5nLCBUw7pjIER1ecOqbiwgVGjDoG5oIHBo4buRIFRow6FpIE5ndXnDqm4sIFRow6FpIE5ndXnDqm4!5e0!3m2!1svi!2s!4v1516092164743" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>
    </div>
    <div class="container">
        <div id="content" class="space-top-none">

            <div class="space50">&nbsp;</div>
            <div class="row">
                <div class="col-sm-8">
                    <h2>Liên hệ</h2>
                    <div class="space20">&nbsp;</div>
                    <p>Mọi thắc mắc vui lòng bạn gửi mail cho chúng tôi</p>
                    <div class="space20">&nbsp;</div>
                    <form action="{{route('lienhe')}}" method="post" class="contact-form" id="formlh">
                        {{csrf_field()}}
                        <div class="form-block">
                            <input name="email" type="text" placeholder="email">
                        </div>
                        <div class="form-block">
                            <input name="subject" type="text" placeholder="chủ đề">
                        </div>
                        <div class="form-block">
                            <textarea name="message" placeholder="nội dung"></textarea>
                        </div>
                        <div class="form-block">
                            <button type="submit" class="beta-btn primary">Gửi tin nhắn<i class="fa fa-chevron-right"></i></button>
                        </div>
                    </form>
                </div>
                <div class="col-sm-4">
                    <h2>Thông tin liên hệ</h2>

                    <div class="space20">&nbsp;</div>

                    <h6 class="contact-title">Địa chỉ:</h6>
                    <p> Tổ 28, P.Phan Đình Phùng, TP.Thái Nguyên
                    </p>
                    <div class="space20">&nbsp;</div>
                    <h6 class="contact-title">Điện thoại:</h6>
                    <p> 0868 603 832
                    </p>
                    <div class="space20">&nbsp;</div>
                </div>
            </div>
        </div> <!-- #content -->
    </div> <!-- .container -->
@endsection