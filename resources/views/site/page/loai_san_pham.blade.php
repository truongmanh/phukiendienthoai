@extends('site.master')
@section('title','Loại sản phẩm')
@section('content')
    <div class="inner-header">
        <div class="container">
            <div class="pull-left">
                <h6 class="inner-title">Sản phẩm</h6>
            </div>
            <div class="pull-right">
                <div class="beta-breadcrumb font-large">
                    <a href="{{route('trang-chu')}}">Trang chủ</a> / <span>Sản phẩm</span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="container">
        <div id="content" class="space-top-none">
            <div class="main-content">
                {{--<div class="space60">&nbsp;</div>--}}
                <div class="row">
                    <div class="col-sm-3">

                        <div class="panel panel-primary">
                            <div class="panel-heading">Danh mục</div>
                            <div class="panel-body">
                                <ul class="aside-menu">
                                    @foreach($loai as $item)
                                        <li><a href="{{route('loaisanpham',[$item->id,$item->alias])}}">{{$item->name}}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="panel panel-primary">
                            <div class="panel-heading">Lựa chọn</div>
                            <div class="panel-body">
                                <form action="">
                                    <div class="form-group">
                                        <div class="radio">
                                            <label><input type="radio" name="optradio">Iphone</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="radio">
                                            <label><input type="radio" name="optradio">Samsung</label>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="beta-products-list">
                            <h4>{{$loai_ten->name}}</h4>
                            <div class="space60">&nbsp;</div>
                            <div class="row">
                            @foreach($loaisp as $item)
                                <div class="col-sm-4">
                                    <div class="single-item">
                                        @if($item->promotion_price!=0)
                                            <div class="ribbon-wrapper"><div class="ribbon sale">Sale</div></div>
                                        @endif
                                        <div class="single-item-header">
                                            <a href="{{route('chitietsanpham',[$item->id,$item->alias])}}"><img src="uploads/product/{{$item->image}}" height="250px" alt=""></a>
                                        </div>
                                        <div class="single-item-body">
                                            <p class="single-item-title">{{$item->name}}</p>
                                            <p class="single-item-price">
                                                @if($item->promotion_price==0)
                                                    <span>{{number_format($item->unit_price,3,',','.')}}vnđ</span>
                                                @else
                                                    <span class="flash-del">{{number_format($item->unit_price,3,',','.')}}vnđ</span>
                                                    <span class="flash-sale">{{number_format($item->promotion_price,3,',','.')}}vnđ</span>
                                                @endif
                                            </p>
                                        </div>
                                        <div class="single-item-caption">
                                            <a class="add-to-cart pull-left" href="{{route('themgiohang',$item->id)}}"><i class="fa fa-shopping-cart"></i></a>
                                            <a class="beta-btn primary" href="{{route('chitietsanpham',[$item->id,$item->alias])}}">Chi tiết<i class="fa fa-chevron-right"></i></a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                                    @endforeach()
                            </div>
                            <div class="row">
                            <div class="pull-right">{{$loaisp->links()}}</div></div>
                        </div> <!-- .beta-products-list -->

                        <div class="space50">&nbsp;</div>
                    </div>
                </div> <!-- end section with sidebar and main content -->


            </div> <!-- .main-content -->
        </div> <!-- #content -->
    </div>
    @endsection