@extends('site.master')
@section('content')
    <div class="inner-header">
        <div class="container">
            <div class="pull-left">
                <h6 class="inner-title">Đăng kí</h6>
            </div>
            <div class="pull-right">
                <div class="beta-breadcrumb">
                    <a href="{{route('trang-chu')}}">Home</a> / <span>Đăng kí</span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    @if(Session::has('thong bao'))
        <div class="errors-login">
            <h5>{{Session::get('thong bao')}}</h5>
        </div>
    @endif
    <div class="container">
        <div id="content">

            <form action="{{route('dangky')}}" method="post" class="beta-form-checkout" id="formdk">
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6">
                        <h4>Đăng kí</h4>
                        <div class="space20">&nbsp;</div>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        {{--{{ csrf_field() }}--}}
                        <div class="form-block">
                            <label for="email">Email address*</label>
                            <input type="email" name="email" required>
                        </div>

                        <div class="form-block">
                            <label for="your_last_name">Fullname*</label>
                            <input type="text" name="name" id="your_last_name" required>
                        </div>

                        <div class="form-block">
                            <label for="phone">Password*</label>
                            <input type="password" name="pass" required>
                        </div>
                        <div class="form-block">
                            <button type="submit" class="btn btn-primary">Đăng ký</button>
                        </div>
                    </div>
                    <div class="col-sm-3"></div>
                </div>
            </form>
        </div> <!-- #content -->
    </div> <!-- .container -->
    @endsection()