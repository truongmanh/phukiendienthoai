<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'trang-chu', 'uses' => 'PageController@getIndex']);
Route::get('index', ['as' => 'trang-chu', 'uses' => 'PageController@getIndex']);
Route::get('loai-san-pham/{id}/{slug?}', ['as' => 'loaisanpham', 'uses' => 'PageController@getTypeSp']);
Route::get('chi-tiet-san-pham/{id}/{slug?}', ['as' => 'chitietsanpham', 'uses' => 'PageController@getDetailSp']);
Route::get('lien-he', ['as' => 'lien-he', 'uses' => 'PageController@getContact']);
Route::post('lien-he', ['as' => 'lienhe', 'uses' => 'PageController@postContact']);
Route::get('add-to-cart/{id}', ['as' => 'themgiohang', 'uses' => 'PageController@getAddCart']);
Route::get('del-cart-one/{id}', ['as' => 'xoaonegiohang', 'uses' => 'PageController@getDelCartOne']);
Route::get('del-cart/{id}', ['as' => 'xoagiohang', 'uses' => 'PageController@getDelCart']);
Route::get('dang-ky', ['as' => 'dangky', 'uses' => 'PageController@getdangky']);
Route::post('dang-ky', ['as' => 'dangky', 'uses' => 'PageController@postdangky']);
Route::get('dang-nhap', ['as' => 'dangnhap', 'uses' => 'PageController@getdangnhap']);
Route::post('dang-nhap', ['as' => 'dangnhap', 'uses' => 'PageController@postdangnhap']);
Route::get('dang-xuat', ['as' => 'dangxuat', 'uses' => 'PageController@postdangxuat']);
Route::get('check-out', ['as' => 'checkout', 'uses' => 'PageController@getCheckout']);
Route::post('check-out', ['as' => 'checkout', 'uses' => 'PageController@postCheckout']);
Route::get('Search-Product', ['as' => 'Search-Product', 'uses' => 'PageController@getTimKiem']);
Route::get('Gioi-thieu', ['as' => 'gioi-thieu', 'uses' => 'PageController@getGioithieu']);
Route::get('wishlist', ['as' => 'quan-tam', 'uses' => 'WishlistController@getIndex']);
Route::get('addwishlist/{id}', ['as' => 'add-wishlist', 'uses' => 'WishlistController@addwishlist']);
Route::get('delwishlist/{id}', ['as' => 'del-wishlist', 'uses' => 'WishlistController@delwishlist']);
Route::get('profile/{id}',['as'=>'profile','uses'=>'ProfileController@getIndex']);
//login with facebook
Route::get('facebook/redirect', ['as' => 'facebook/redirect', 'uses' => 'Auth\SocialController@redirectToProvider']);
Route::get('facebook/callback', ['as' => 'facebook/callback', 'uses' => 'Auth\SocialController@handleProviderCallback']);
 
Route::get('admin/login', ['as' => 'login', 'uses' => 'AdminController@getIndex']);
Route::post('admin/login', ['as' => 'login', 'uses' => 'AdminController@postLogin']);
Route::get('logout', ['as' => 'logout', 'uses' => 'AdminController@getLogout']);
Route::group(['middleware'=>'auth:admin'],function(){
    Route::group(['prefix' => 'admin'], function () {
//        Route::get('/',["uses"=>"AdminController@getIndex"]);
        Route::group(['prefix' => 'cate'], function () {
            Route::get('list-cate', ['as' => 'getListCate', 'uses' => 'CategoryController@getListCate']);
            Route::get('add-cate', ['as' => 'getAddCate', 'uses' => 'CategoryController@getAddCate']);
            Route::post('add-cate', ['as' => 'postAddCate', 'uses' => 'CategoryController@postAddCate']);
            Route::get('edit-cate/{id}', ['as' => 'getEditCate', 'uses' => 'CategoryController@getEditCate'])->where('id', '[0-9]+');
            Route::post('edit-cate/{id}', ['as' => 'postEditCate', 'uses' => 'CategoryController@postEditCate'])->where('id', '[0-9]+');
            Route::get('del-cate/{id}', ['as' => 'getDelCate', 'uses' => 'CategoryController@getDelCate'])->where('id', '[0-9]+');
        });
    Route::group(['prefix' => 'product'], function () {
        Route::get('list-product', ['as' => 'getListProduct', 'uses' => 'ProductController@getListProduct']);
        Route::get('add-product', ['as' => 'getAddProduct', 'uses' => 'ProductController@getAddProduct']);
        Route::post('add-product', ['as' => 'postAddProduct', 'uses' => 'ProductController@postAddProduct']);
        Route::get('edit-product/{id}', ['as' => 'getEditProduct', 'uses' => 'ProductController@getEditProduct'])->where('id', '[0-9]+');
        Route::post('edit-product/{id}', ['as' => 'postEditProduct', 'uses' => 'ProductController@postEditProduct'])->where('id', '[0-9]+');
        Route::get('del-product/{id}', ['as' => 'getDelProduct', 'uses' => 'ProductController@getDelProduct'])->where('id', '[0-9]+');
    });
        Route::group(['prefix' => 'slider'], function () {
            Route::get('list-slider', ['as' => 'getListSlider', 'uses' => 'SliderController@getListSlider']);
            Route::get('add-slider', ['as' => 'getAddSlider', 'uses' => 'SliderController@getAddSlider']);
            Route::post('add-slider', ['as' => 'postAddSlider', 'uses' => 'SliderController@postAddSlider']);
            Route::get('edit-slider/{id}', ['as' => 'getEditSlider', 'uses' => 'SliderController@getEditSlider'])->where('id', '[0-9]+');
            Route::post('edit-slider/{id}', ['as' => 'postEditSlider', 'uses' => 'SliderController@postEditSlider'])->where('id', '[0-9]+');
            Route::get('del-slider/{id}', ['as' => 'getDelSlider', 'uses' => 'SliderController@getDelSlider'])->where('id', '[0-9]+');
        });
   Route::group(['prefix' => 'user'], function () {
       Route::get('list-admin', ['as' => 'getListAdmin', 'uses' => 'AdminController@getListAdmin']);
       Route::get('list-user', ['as' => 'getListUser', 'uses' => 'AdminController@getListUser']);
       Route::get('add-user', ['as' => 'getAddUser', 'uses' => 'AdminController@getAddUser']);
       Route::post('add-user', ['as' => 'postAddUser', 'uses' => 'AdminController@postAddUser']);
       Route::get('del-user/{id}', ['as' => 'getDelUser', 'uses' => 'AdminController@getDelUser'])->where('id', '[0-9]+');
       Route::get('del-admin/{id}', ['as' => 'getDelAdmin', 'uses' => 'AdminController@getDelAdmin'])->where('id', '[0-9]+');
   });
   Route::group(['prefix' => 'customer'], function () {
    Route::get('list-customer', ['as' => 'getListCustomer', 'uses' => 'AdminController@getListCustomer']);
    });
    Route::group(['prefix' => 'bill'], function () {
        Route::get('list-bill', ['as' => 'getListBill', 'uses' => 'AdminController@getListBill']);
        Route::get('edit-bill/{id}', ['as' => 'getEditBill', 'uses' => 'AdminController@getEditBill']);
        Route::post('edit-bill/{id}', ['as' => 'postEditBill', 'uses' => 'AdminController@postEditBill']);
        });
    });
});

