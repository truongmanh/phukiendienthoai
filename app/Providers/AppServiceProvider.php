<?php

namespace App\Providers;

use App\Bill;
use App\Cart;
use App\Products;
use App\type_product;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Session;
use DateTime;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        view()->composer('site.header',function($view){
                $loaisp=type_product::where('status',1)->get();
                $view->with('loaisp',$loaisp);
        });

        view()->composer(['site.header','site.page.dathang'],function($view){
         if(Session('cart'))
         {
          $oldCart=Session::get('cart');
          $cart=new Cart($oldCart);
             $view->with([
                 'cart'=>Session::get('cart'),
                 'product_cart'=>$cart->items,
                 'totalPrice'=>$cart->totalPrice,
                 'totalQty'=>$cart->totalQty
             ]);
         }

        });

        view()->composer('admin.bill.list',function($view){
            $dt = new DateTime();
            $data_now= $dt->format('Y-m-d');
            $total_bill=Bill::sum('total');
            $data_bill=Bill::all();
            $data_bill_all=Products::all();
            $total_bill_all=Products::sum('unit_price');

            $bill_thang=Bill::whereMonth('date_order',date('m'))->get();
            $total_bill_thang=Bill::whereMonth('date_order',date('m'))->sum('total');

            $bill_thang_all=Products::whereMonth('created_at',date('m'))->get();
            $total_bill_thang_all=Products::whereMonth('created_at',date('m'))->sum('unit_price');

            $bill_today=Bill::whereDate('date_order', $data_now)->get();
            $total_bill_today=Bill::whereDate('date_order', $data_now)->sum('total');
            $view->with([
                'data_bill'=>$data_bill,
                'total_bill'=>$total_bill,
               'data_bill_all'=> $data_bill_all,
                'total_bill_all'=>$total_bill_all,
                'bill_thang'=>$bill_thang,
                'total_bill_thang'=>$total_bill_thang,
                'bill_thang_all'=>$bill_thang_all,
                'total_bill_thang_all'=>$total_bill_thang_all,
                'bill_today'=>$bill_today,
                'total_bill_today'=>$total_bill_today]);
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
