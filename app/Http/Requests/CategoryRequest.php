<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txt_name'=>'required|unique:type_products,name',
            'txt_des'=>'required',
            'txt_image'=>'mimes:jpeg,jpg,png'
        ];
    }
    public function messages()
    {
        return[
           'txt_name.required'=>'Tên không được để trống',
           'txt_des.required'=>'Miêu tả không được để trống',
           'txt_name.unique'=>'Tên không được trùng',
           'txt_image.image'=>'phải là ảnh',
        ];
    }
}
